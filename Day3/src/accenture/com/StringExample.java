package accenture.com;
public class StringExample {
	public static void main(String[] args) {
		System.out.println("string example"); // "string example" object will be created and placed in heap memory
												// (string pool)
		String name = "dadaram jadhav"; // internally it will array of character and store your name
		String address = new String("pune"); // internally it will array of character and store your address

		System.out.println(name);
		System.out.println(address);
		System.out.println(name.length());
		System.out.println(name.charAt(10));
		System.out.println(name.indexOf("j"));
		System.out.println(address.toUpperCase());
		System.out.println(address.toLowerCase());
		System.out.println(name.substring(8));
		System.out.println(name.concat(" working in accenture"));
		System.out.println(address.contains("n"));
		System.out.println(address.equals("pune123"));
		System.out.println(address.equals("Pune"));
		System.out.println(address.equalsIgnoreCase("Pune"));
		
		//todo --> explore more functions from string class
		
		
		//creating stringbuffer and stringbuilder
		StringBuffer buffer = new StringBuffer("dm");
		buffer.append("jadhav");
		buffer.append(" from pune");
		
		System.out.println("string buffer: " + buffer);
		
		StringBuilder builder = new StringBuilder("accenture");
		builder.append(" india");
		builder.append(" having 10 locations ");		
		System.out.println(builder);
	}
}

